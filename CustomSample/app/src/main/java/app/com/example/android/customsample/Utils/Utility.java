package app.com.example.android.customsample.Utils;

import android.content.Context;

import com.google.gson.Gson;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;

import app.com.example.android.customsample.Entity.LocationData;

/**
 * Created by Krishna Upadhya on 6/16/2017.
 */

public class Utility {

    private static LocationData getConfFromAssets(Context context) {
        InputStream inputStream = null;
        try {
            inputStream = context.getAssets().open(Constants.LOCATION_ASSETS);
            Reader reader = new InputStreamReader(inputStream);
            Gson gson = new Gson();
            LocationData locationData = gson.fromJson(reader, LocationData.class);
            return locationData;
        } catch (IOException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (inputStream != null) {
                try {
                    inputStream.close();
                    inputStream = null;
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return null;
    }

}
