package app.com.example.android.customsample;

import android.app.Application;
import android.content.Context;

import app.com.example.android.customsample.dagger.AppComponent;
import app.com.example.android.customsample.dagger.AppModule;
import app.com.example.android.customsample.dagger.DaggerAppComponent;

/**
 * Created by Krishna Upadhya on 6/1/2017.
 */

public class CustomApplication extends Application {

    private static Context context;
    private AppComponent mAppComponent;

    @Override
    public void onCreate() {
        super.onCreate();
        mAppComponent = DaggerAppComponent.builder()
                .appModule(new AppModule(this))
                .build();
    }

    public AppComponent getRSAppComponent() {
        return mAppComponent;
    }

    public CustomApplication() {
        context = this;
    }

    public static Context getContext() {
        return context;
    }

}
