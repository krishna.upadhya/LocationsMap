package app.com.example.android.customsample.dagger;

import javax.inject.Singleton;

import app.com.example.android.customsample.CustomApplication;
import dagger.Module;
import dagger.Provides;

/**
 * Created by Krishna Upadhya on 6/1/2017.
 */

@Module
public class AppModule {

    private CustomApplication customApplication;

    public AppModule(CustomApplication application) {
        customApplication = application;
    }


    @Provides
    @Singleton
    CustomApplication provideApplication() {
        return customApplication;
    }

}
