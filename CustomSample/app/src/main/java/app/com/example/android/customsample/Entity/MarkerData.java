package app.com.example.android.customsample.Entity;

import com.google.android.gms.maps.model.LatLng;

import java.io.Serializable;

/**
 * Created by Krishna Upadhya on 6/16/2017.
 */
public class MarkerData implements Serializable {

    private String title;

    private LatLng latLong;

    public LatLng getLatLong() {
        return latLong;
    }

    public void setLatLong(LatLng latLong) {
        this.latLong = latLong;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
