package app.com.example.android.customsample.Entity;

/**
 * Created by Krishna Upadhya on 6/16/2017.
 */

public class Locations {
    private String title;

    private String image_url;

    private String spippet;

    private String longitude;

    private String latitude;

    public String getTitle ()
    {
        return title;
    }

    public void setTitle (String title)
    {
        this.title = title;
    }

    public String getImage_url ()
    {
        return image_url;
    }

    public void setImage_url (String image_url)
    {
        this.image_url = image_url;
    }

    public String getSpippet ()
    {
        return spippet;
    }

    public void setSpippet (String spippet)
    {
        this.spippet = spippet;
    }

    public String getLongitude ()
    {
        return longitude;
    }

    public void setLongitude (String longitude)
    {
        this.longitude = longitude;
    }

    public String getLatitude ()
    {
        return latitude;
    }

    public void setLatitude (String latitude)
    {
        this.latitude = latitude;
    }

}
