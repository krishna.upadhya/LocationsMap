package app.com.example.android.customsample.Activity;

import android.Manifest;
import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.ArrayList;

import app.com.example.android.customsample.Entity.MarkerData;
import app.com.example.android.customsample.R;

public class MainActivity extends AppCompatActivity implements OnMapReadyCallback {

    static final LatLng MUMBAI = new LatLng(19.093, 72.880);
    static final LatLng BANGLORE = new LatLng(13.026, 77.594);
    static final LatLng DELHI = new LatLng(28.758, 77.081);
    static final LatLng CHENNAI = new LatLng(13.108, 80.273);
    public static final int MY_PERMISSIONS_REQUEST_LOCATION = 99;

    private GoogleMap map;
    ArrayList<MarkerData> markersArray = new ArrayList<MarkerData>();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        MarkerData data = new MarkerData();
        data.setTitle("BANGLORE");
        data.setLatLong(BANGLORE);
        markersArray.add(data);
        data = new MarkerData();
        data.setTitle("MUMBAI");
        data.setLatLong(MUMBAI);
        markersArray.add(data);
        data = new MarkerData();
        data.setTitle("DELHI");
        data.setLatLong(DELHI);
        markersArray.add(data);
        data = new MarkerData();
        data.setTitle("CHENNAI");
        data.setLatLong(CHENNAI);
        markersArray.add(data);
        MapFragment mapFragment = (MapFragment) getFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);


    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        map = googleMap;
        if (checkLocationPermission())
            setUpMap();
    }

    private void setUpMap() {
        for (int i = 0; i < markersArray.size(); i++) {
            createMarker(markersArray.get(i).getLatLong(), markersArray.get(i).getTitle());
        }
//        setUpMap(map, KIEL, "one");
//        setUpMap(map, HAMBURG, "two");
        //map.moveCamera( CameraUpdateFactory.newLatLngZoom(KIEL , 14.0f) );
        map.moveCamera(CameraUpdateFactory.newLatLng(BANGLORE));
    }

    protected Marker createMarker(LatLng latLng, String title) {

        return map.addMarker(new MarkerOptions()
                .position(latLng)
                .anchor(0.5f, 0.5f)
                .title(title)
                .snippet(title + " place"));

    }

    public boolean checkLocationPermission() {
        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {

            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                    Manifest.permission.ACCESS_FINE_LOCATION)) {

                // Show an explanation to the user *asynchronously* -- don't block
                // this thread waiting for the user's response! After the user
                // sees the explanation, try again to request the permission.
                new AlertDialog.Builder(this)
                        .setTitle(R.string.title_location_permission)
                        .setMessage(R.string.text_location_permission)
                        .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                //Prompt the user once explanation has been shown
                                ActivityCompat.requestPermissions(MainActivity.this,
                                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                                        MY_PERMISSIONS_REQUEST_LOCATION);
                            }
                        })
                        .create()
                        .show();


            } else {
                // No explanation needed, we can request the permission.
                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                        MY_PERMISSIONS_REQUEST_LOCATION);
            }
            return false;
        } else {
            return true;
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_LOCATION: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    // permission was granted, yay! Do the
                    // location-related task you need to do.
                    if (ContextCompat.checkSelfPermission(this,
                            Manifest.permission.ACCESS_FINE_LOCATION)
                            == PackageManager.PERMISSION_GRANTED) {

                        setUpMap();

                    }

                } else {

                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.

                }
                return;
            }

        }
    }

}
