package app.com.example.android.customsample.dagger;

import app.com.example.android.customsample.Activity.MainActivity;
import dagger.Component;

/**
 * Created by Krishna Upadhya on 6/1/2017.
 */


@Component(modules = {AppModule.class})
public interface AppComponent {
    void inject(MainActivity mainActivity);
}
