package app.com.example.android.customsample.Entity;

import java.util.ArrayList;

/**
 * Created by Krishna Upadhya on 6/16/2017.
 */

public class LocationData extends BaseDataModel {
    private ArrayList<Locations> locationsList;


    public ArrayList<Locations> getLocationsList() {
        return locationsList;
    }

    public void setLocationsList(ArrayList<Locations> locationsList) {
        this.locationsList = locationsList;
    }
}
